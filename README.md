Color Spotter Game
===============
This repo is a single page application for Color Spotter Game. You can play the game on [https://cekici.gitlab.io/color-spotter](https://cekici.gitlab.io/color-spotter)

## How To Build
```
$ npm install
$ npm run build
```

## How To Run
```
$ npm run start
```

Then in your browser navigate to [http://localhost:8080/color-spotter](http://localhost:8080/color-spotter) for the Color Spotter Game.

## How To Use
If you click on "Game" button, you can see tiles. Click on the tile with different color to pass the level. If you click wrong tile, the game ends.


## Frameworks Used
- [React](https://facebook.github.io/react/) is used for UI components. 
- [Redux](http://redux.js.org/) is used for state management.
- [Webpack](https://webpack.js.org) is used for build tasks.
- [Babel](https://babeljs.io/) is used for compiling next generation JS to current JS supported for browsers.
- [SASS](http://sass-lang.com/) is used for writing modular CSS.
- [JEST](https://facebook.github.io/jest/) is used for testing.


## Notes
You can see that there are two buttons injected into the Web App as __Hint__ & __Solve Level__. Those buttons are injected by solver.js file which is a standalone game solver, living separately from the app.