import React, {Component} from "react";
import {BrowserRouter as Router, Switch, Route, Redirect, withRouter} from "react-router-dom";
import {connect} from "react-redux";

import * as ROUTE_PATHS from "../constants/routePaths.js";
import DashboardPageContent from "../pages/dashboard/DashboardPageContent.jsx";
import GamePageContent from "../pages/game/GamePageContent.jsx";
import RankingPageContent from "../pages/ranking/RankingPageContent.jsx";

import "./_app.scss";

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact
               path={ROUTE_PATHS.ROUTE_DASHBOARD}
               component={DashboardPageContent}/>

        <Route path={ROUTE_PATHS.ROUTE_GAME}
               component={GamePageContent}/>

        <Route path={ROUTE_PATHS.ROUTE_RANKING}
               component={RankingPageContent}/>

        <Redirect from="/*"
                  to="/"/>
      </Switch>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const {
    gameState: {
      score
    },
  } = state;

  return {
    score
  };
}

const ConnectedApp = connect(mapStateToProps)(App);
const ConnectedAppWithRouter = withRouter(ConnectedApp);

class RootConnectedApp extends Component {
  render() {
    return (
      <Router basename="/color-spotter">
        <Route component={ConnectedAppWithRouter}/>
      </Router>
    );
  }
}

export default RootConnectedApp;


