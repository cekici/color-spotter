import React from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import store from "./core/app/appStore.js";
import App from "./app/App.jsx";

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.querySelector("#app"));

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('./service-worker.js');
  });
}
