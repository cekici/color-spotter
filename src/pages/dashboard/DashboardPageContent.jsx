import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";

import {ROUTE_GAME, ROUTE_RANKING} from "../../constants/routePaths.js";

import "./_dashboard-page-content.scss";

class DashboardPageContent extends Component {
  render() {
    return (
      <div className="cs-dashboard-page">
        <h1 className="cs-dashboard-title">{"Color Spotter"}</h1>

        <div className="cs-dashboard-button-container">
          <Link to={ROUTE_GAME}
                className="cs-dashboard-page-link">
            <button className="cs-dashboard-page-game-button">{"Game"}</button>
          </Link>

          <Link to={ROUTE_RANKING}
                className="cs-dashboard-page-link">
            <button className="cs-dashboard-page-ranking-button">{"Ranking"}</button>
          </Link>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const {
    gameState: {
      score
    },
  } = state;

  return {
    score
  };
}

const ConnectedDashboardPageContent = connect(mapStateToProps)(DashboardPageContent);
const ConnectedDashboardPageContentWithRouter = withRouter(ConnectedDashboardPageContent);

export default ConnectedDashboardPageContentWithRouter;