import React, {Component} from "react";
import {Link, withRouter} from "react-router-dom";
import {connect} from "react-redux";

import * as rankingActions from "../../core/ranking/core/rankingActions.js";
import * as gameActions from "../../core/game/core/gameActions.js";
import {ROUTE_DASHBOARD} from "../../constants/routePaths.js";
import RankingList from "../../components/ranking/list/RankingList.jsx";
import RankingForm from "../../components/ranking/form/RankingForm.jsx";

import "./_ranking-page-content.scss";

class RankingPageContent extends Component {
  constructor(props) {
    super(props);

    const {finalScore} = props;

    this.state = {
      score: finalScore,
      shouldDisplayRankingForm: false,
      shouldDisplayRankingTable: false
    }
  }

  componentWillMount() {
    const {dispatch} = this.props;
    const {score} = this.state;


    if(score) {
      dispatch(gameActions.resetFinalScore());
      dispatch(rankingActions.checkRankingEligibility(score));
    } else {
      this.setState({
        shouldDisplayRankingTable: true
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    const {isRankingEligible} = this.props;

    if (nextProps.isRankingEligible !== isRankingEligible) {

      if (nextProps.isRankingEligible) {
        this.setState({
          shouldDisplayRankingForm: true
        })
      } else {
        this.setState({
          shouldDisplayRankingTable: true
        })
      }
    }
  }

  handleDismissRankingForm = () => {
    this.setState({
      shouldDisplayRankingForm: false,
      shouldDisplayRankingTable: true
    });
  }

  handleSubmitRankingForm = (name) => {
    const {dispatch} = this.props;
    const {score} = this.state;

    this.setState({
      shouldDisplayRankingForm: false,
      shouldDisplayRankingTable: true
    });

    dispatch(rankingActions.updateRankingList({name: name, score: score}));
  }

  render() {
    const {rankingList} = this.props;
    const {score, shouldDisplayRankingForm, shouldDisplayRankingTable} = this.state;

    return (
      <div className="cs-ranking-page">

        {score &&
        <span className="cs-ranking-page-score-text">{`Your Score: ${score}`}</span>
        }

        {shouldDisplayRankingForm &&
        <RankingForm onDismiss={this.handleDismissRankingForm}
                     onSubmit={this.handleSubmitRankingForm}/>
        }

        {shouldDisplayRankingTable &&
        <RankingList rankingList={rankingList}/>
        }

        {shouldDisplayRankingTable &&
        <div className="cs-ranking-button-container">
          <Link to={ROUTE_DASHBOARD}>
            <button className="cs-ranking-page-new-game-button">{"Start Over"}</button>
          </Link>
        </div>
        }
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const {
    gameState: {
      finalScore
    },
    rankingState: {
      rankingList,
      isRankingEligible
    }
  } = state;

  return {
    finalScore,
    rankingList,
    isRankingEligible
  };
}

const ConnectedRankingPageContent = connect(mapStateToProps)(RankingPageContent);
const ConnectedRankingPageContentWithRouter = withRouter(ConnectedRankingPageContent);

export default ConnectedRankingPageContentWithRouter;