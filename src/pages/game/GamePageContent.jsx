import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";

import Game from "../../components/game/Game.jsx";
import * as gameActions from "../../core/game/core/gameActions.js";
import {ROUTE_RANKING} from "../../constants/routePaths.js";

import "./_game-page-content.scss";

class GamePageContent extends Component {
  componentWillMount() {
    const {dispatch} = this.props;

    dispatch(gameActions.startGame());
  }

  componentWillReceiveProps(nextProps) {
    const {dispatch, isGameActive} = this.props;

    if(!nextProps.isGameActive && isGameActive) {
      const {history} = this.props;

      dispatch(gameActions.endGame());
      history.push(ROUTE_RANKING);
    }
  }

  handleTileClick = (tile) => {
    const {dispatch} = this.props;
    dispatch(gameActions.guessTile(tile));
  }

  render() {
    const {score, level, tileList} = this.props;

    return (
      <div className="cs-game-page">
        <Game score={score}
              level={level}
              tileList={tileList}
              onTileClick={this.handleTileClick}/>
      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const {
    gameState: {
      score,
      level,
      isGameActive,
      tileList
    },
  } = state;

  return {
    score,
    level,
    isGameActive,
    tileList
  };
}

const ConnectedGamePageContent = connect(mapStateToProps)(GamePageContent);
const ConnectedGamePageContentWithRouter = withRouter(ConnectedGamePageContent);

export default ConnectedGamePageContentWithRouter;