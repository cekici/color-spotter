var solveButton = document.createElement("button");
solveButton.id = "cs-solve-button";
solveButton.textContent = "Solve Level";
document.body.appendChild(solveButton);

var showHintButton = document.createElement("button");
showHintButton.id = "cs-show-hint-button";
showHintButton.textContent = "Show Hint";
document.body.appendChild(showHintButton);

function findTileContent(tile) {
  return tile.children[0];
}

function getBackground(el) {
  return el.style.background;
}

function findTile() {
  var tileEls =  document.querySelectorAll(".cs-tile");
  var firstTileBg = getBackground(findTileContent(tileEls[0]));
  var secondTileBg = getBackground(findTileContent(tileEls[1]));
  var tile;

  if(firstTileBg === secondTileBg) {
    for(var i = 2; i < tileEls.length; i++) {
      var tileBg = getBackground(findTileContent(tileEls[i]));

      if(tileBg !== firstTileBg) {
        tile = tileEls[i];
        break;
      }
    }
  } else {
    var thirdTileBg = getBackground(findTileContent(tileEls[2]));

    if(thirdTileBg === firstTileBg) {
      tile = tileEls[1];
    } else if(thirdTileBg === secondTileBg) {
      tile = tileEls[0];
    }
  }

  return tile;
}

solveButton.addEventListener("click", function() {
  var boardEl = document.querySelector(".cs-board");

  if(boardEl) {
    var tile = findTile();
    tile.click();
  }
});


showHintButton.addEventListener("click", function() {
  var boardEl = document.querySelector(".cs-board");

  if(boardEl) {
    var tile = findTile();
    var tileContent = findTileContent(tile);

    tileContent.style.background = "repeating-linear-gradient(135deg, white 0%, white 25%, black 0%, black 50%) 0/15px 15px";
  }
});