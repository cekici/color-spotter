import React, {Component} from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import "./_ranking-form.scss";

export default class RankingForm extends Component {
  static propTypes = {
    onDismiss: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      name: "",
      hasError: false
    }
  }

  handleNameChange = (event) => {
    this.setState({
      hasError: false,
      name: event.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {onSubmit} = this.props;
    const {name} = this.state;

    if (!name) {
      this.setState({
        hasError: true
      });
    } else {
      onSubmit(name);
    }
  }

  render() {
    const {onDismiss} = this.props;
    const {name, hasError} = this.state;

    const inputClassName = classNames("cs-ranking-form-name-input", {
      "has-error": hasError
    });

    return (
      <form className="cs-ranking-form"
            onSubmit={this.handleSubmit}>

        <input name="name"
               value={name}
               placeholder="Enter your name"
               className={inputClassName}
               onChange={this.handleNameChange}/>

        {hasError &&
        <span className="cs-ranking-form-error">{"Please enter your name"}</span>
        }

        <div className="cs-ranking-form-button-container">
          <button type="button"
                  className="cs-ranking-form-dismiss-button"
                  onClick={onDismiss}>
            {"Cancel"}
          </button>
          <button className="cs-ranking-form-submit-button"
                  type="submit">
            {"Submit"}
          </button>
        </div>
      </form>
    );
  }
}
