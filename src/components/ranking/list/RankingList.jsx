import React, {Component} from "react";
import PropTypes from "prop-types";

import "./_ranking-list.scss";

export default class RankingList extends Component {
  static propTypes = {
    rankingList: PropTypes.array.isRequired
  }

  render() {
    const {rankingList} = this.props;

    return (
      <table className="cs-ranking-list">
        <caption className="cs-ranking-table-title">{`Top ${rankingList.length}`}</caption>

        <thead>
          <tr>
            <th className="cs-ranking-list-header-cell">{"Name"}</th>
            <th className="cs-ranking-list-header-cell">{"Score"}</th>
          </tr>
        </thead>
        <tbody>
        {rankingList.map((ranking, index) =>
        <tr key={index}>
          <td className="cs-ranking-list-body-cell">{ranking.name}</td>
          <td className="cs-ranking-list-body-cell">{ranking.score}</td>
        </tr>
        )}
        </tbody>
      </table>
    );
  }
}
