import React, {Component} from "react";
import PropTypes from "prop-types";

import Tile from "../tile/Tile.jsx";

import "./_board.scss";

export default class Board extends Component {
  static propTypes = {
    tileList: PropTypes.array.isRequired,
    level: PropTypes.number.isRequired,
    onTileClick: PropTypes.func.isRequired
  }

  render() {
    const {tileList, level, onTileClick} = this.props;

    return (
      <div className="cs-board">
        {tileList.map((tile, index) =>
          <Tile tile={tile}
                level={level}
                onClick={onTileClick}
                key={index}/>
        )}
      </div>
    );
  }
}
