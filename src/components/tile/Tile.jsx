import React, {Component} from "react";
import PropTypes from "prop-types";

import "./_tile.scss";

export default class Tile extends Component {
  static propTypes = {
    tile: PropTypes.object.isRequired,
    level: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired
  }

  handleClick = () => {
    const {tile, onClick} = this.props;

    onClick(tile);
  }

  render() {
    const {tile, level} = this.props;
    const {color} = tile;

    const size = 100 / level;

    const tileStyle = {
      width: `${size}%`
    };

    const tileContentStyle = {
      background: color
    };

    return (
      <div className="cs-tile"
           style={tileStyle}
           onClick={this.handleClick}>

        <div className="cs-tile-content"
             style={tileContentStyle}></div>
      </div>
    );
  }
}
