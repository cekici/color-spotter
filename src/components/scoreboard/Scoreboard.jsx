import React, {Component} from "react";
import PropTypes from "prop-types";

import "./_scoreboard.scss";

export default class Scoreboard extends Component {
  static propTypes = {
    score: PropTypes.number.isRequired
  }

  render() {
    const {score} = this.props;

    return (
      <div className="cs-scoreboard">
        <h1 className="cs-scoreboard-title">
          {"Score"}
        </h1>
        <span className="cs-scoreboard-score">
          {score}
        </span>
      </div>
    );
  }
}
