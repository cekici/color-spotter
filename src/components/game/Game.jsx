import React, {Component} from "react";
import PropTypes from "prop-types";

import Scoreboard from "../scoreboard/Scoreboard.jsx";
import Board from "../board/Board.jsx";

import "./_game.scss";

export default class Game extends Component {
  static propTypes = {
    score: PropTypes.number.isRequired,
    level: PropTypes.number.isRequired,
    tileList: PropTypes.array.isRequired,
    onTileClick: PropTypes.func.isRequired
  }

  render() {
    const {score, level, tileList, onTileClick} = this.props;

    return (
      <div className="cs-game">
        <Scoreboard score={score}/>

        <Board tileList={tileList}
               level={level}
               onTileClick={onTileClick}/>
      </div>
    );
  }
}

