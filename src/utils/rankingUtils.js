const RANKING_LIST_MAX_SIZE = 10;

function sortNumber(n1, n2) {
  return n2 - n1;
}

export const isEligableToEnterRankingList = (rankingList, score)  => {
  if(rankingList.length < RANKING_LIST_MAX_SIZE) {
    return true;
  }

  return rankingList[RANKING_LIST_MAX_SIZE - 1].score < score;
}

export const updateRankingList = (rankingList, ranking) => {
  rankingList.push(ranking);
  rankingList.sort((item1, item2) => sortNumber(item1.score, item2.score));
  return rankingList.slice(0, RANKING_LIST_MAX_SIZE);
};
