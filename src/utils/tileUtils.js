function getRandom(min, max) {
  return min + Math.random() * (max - min);
}

function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateHSLColor(hue, saturation, lightness) {
  return `hsl(${hue}, ${saturation}%, ${lightness}%`;
}

function getTileColors(level) {
  const hue = getRandom(1, 360);
  const saturation = 80;
  const lightness = 60;
  const differ =  Math.floor(16 * (50 - level) / 50);

  return {
    common: generateHSLColor(hue, saturation, lightness),
    unique: generateHSLColor(hue, saturation + differ, lightness + differ)
  };
}

export const generateTileList = (level) => {
  const tileList = [];
  const {common: commonColor, unique: uniqueColor} = getTileColors(level);

  const noOfTiles = Math.pow(level, 2);
  const uniqueTileIndex = getRandomIntInclusive(0, noOfTiles - 1);

  for(let i = 0; i < noOfTiles; i++) {
    let tile;    
    
    if(i === uniqueTileIndex) {
      tile = {
        isUnique: true,
        color: uniqueColor
      };
    } else {
      tile = {
        isUnique: false,
        color: commonColor
      }; 
    }

    tileList.push(tile);
  }

  return tileList;
};
