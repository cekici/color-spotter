const LOCAL_STORAGE_ITEM_NAME = "color-spotter-state";

export const loadState = () => {
  try {
    const serializedState = localStorage.getItem(LOCAL_STORAGE_ITEM_NAME);

    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(LOCAL_STORAGE_ITEM_NAME, serializedState);
  } catch (e) {

  }
};