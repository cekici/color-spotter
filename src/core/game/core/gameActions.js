import * as ACTIONS from "../../../constants/actionTypes.js";

export const startGame = () => ({
  type: ACTIONS.START_GAME
});

export const guessTile = (tile) => ({
  type: ACTIONS.GUESS_TILE,
  payload: {
    tile: tile
  }
});

export const endGame = () => ({
  type: ACTIONS.END_GAME
});

export const resetFinalScore = () => ({
  type: ACTIONS.RESET_FINAL_SCORE
});