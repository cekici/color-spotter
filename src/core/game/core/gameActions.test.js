import * as ACTIONS from "../../../constants/actionTypes.js";
import * as gameActions from "./gameActions.js";

describe("actions", () => {
  it("should create an action to start game", () => {
    const expectedAction = {
      type: ACTIONS.START_GAME
    }
    expect(gameActions.startGame()).toEqual(expectedAction)
  });

  it("should create an action to guess tile", () => {
    const tile = {
      isUnique: true,
      color: ""
    };
    const expectedAction = {
      type: ACTIONS.GUESS_TILE,
      payload: {
        tile: tile
      }
    };
    expect(gameActions.guessTile(tile)).toEqual(expectedAction)
  });

  it("should create an action to end game", () => {
    const expectedAction = {
      type: ACTIONS.END_GAME
    }
    expect(gameActions.endGame()).toEqual(expectedAction)
  });

  it("should create an action to reset final score", () => {
    const expectedAction = {
      type: ACTIONS.RESET_FINAL_SCORE
    }
    expect(gameActions.resetFinalScore()).toEqual(expectedAction)
  });
});