import * as ACTIONS from "../../../constants/actionTypes.js";
import {generateTileList} from "../../../utils/tileUtils.js";

const defaultInitialState = {
  score: 0,
  level: 2,
  tileList: [],
  isGameActive: false,
  finalScore: null
};

function GameReducer(initialState = defaultInitialState) {
  return function (state = initialState, action) {
    let newState;

    switch (action.type) {
      case ACTIONS.START_GAME:
        newState = {
          ...state,
          isGameActive: true,
          tileList: generateTileList(state.level)
        };
        break;

      case ACTIONS.GUESS_TILE:
        const {tile} = action.payload;

        if (tile.isUnique) {
          const score = state.score + 1;
          const level = state.level + 1;

          newState = {
            ...state,
            score,
            level,
            tileList: generateTileList(level)
          }
        } else {
          newState = {
            ...state,
            isGameActive: false
          }
        }
        break;

      case ACTIONS.END_GAME:
        const finalScore = state.score;

        newState = {
          ...defaultInitialState,
          finalScore
        };
        break;

      case ACTIONS.RESET_FINAL_SCORE:
        newState = {
          ...defaultInitialState
        };
        break;

      default:
        newState = state;
        break;
    }

    return newState;
  }
}

export default GameReducer;
