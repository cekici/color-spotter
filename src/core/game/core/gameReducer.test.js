import * as ACTIONS from "../../../constants/actionTypes.js";
import GameReducer from "./GameReducer.js";

const initialState = {
  score: 0,
  level: 2,
  tileList: [],
  isGameActive: false,
  finalScore: null
};

describe("Game Reducer", () => {
  const reducer = new GameReducer();

  it("should return the initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual(initialState)
  })

  it("should handle START_GAME", () => {
    expect(reducer(initialState, {type: ACTIONS.START_GAME})).toHaveProperty("score", 0)
    expect(reducer(initialState, {type: ACTIONS.START_GAME})).toHaveProperty("level", 2)
    expect(reducer(initialState, {type: ACTIONS.START_GAME})).toHaveProperty("isGameActive", true)
  })

  it("should handle END_GAME", () => {
    expect(reducer({
      ...initialState,
      score: 4,
      level: 6
    }, {
      type: ACTIONS.END_GAME
    }))
      .toEqual({
        ...initialState,
        finalScore: 4
      })
  })

  it("should handle RESET_FINAL_SCORE", () => {
    expect(reducer({
      ...initialState,
      finalScore: 4
    }, {
      type: ACTIONS.RESET_FINAL_SCORE
    }))
      .toEqual(initialState)
  })
})