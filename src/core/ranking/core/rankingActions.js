import * as ACTIONS from "../../../constants/actionTypes.js";

export const  checkRankingEligibility = (score) => ({
  type: ACTIONS.CHECK_RANKING_ELIGIBILITY,
  payload: {
    score
  }
})

export const updateRankingList = (ranking) => ({
  type: ACTIONS.UPDATE_RANKING_LIST,
  payload: {
    ranking
  }
});