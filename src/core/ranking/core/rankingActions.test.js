import * as ACTIONS from "../../../constants/actionTypes.js";
import * as rankingActions from "./rankingActions.js";

describe("actions", () => {
  it("should  create an action to check ranking eligibility", () => {
    const score = 2;

    const expectedAction = {
      type: ACTIONS.CHECK_RANKING_ELIGIBILITY,
      payload: {
        score
      }
    }
    expect(rankingActions.checkRankingEligibility(score)).toEqual(expectedAction)
  });

  it("should create an action to update ranking list", () => {
    const ranking = {
      score: 3,
      name: "Cem"
    };

    const expectedAction = {
      type: ACTIONS.UPDATE_RANKING_LIST,
      payload: {
        ranking
      }
    }
    expect(rankingActions.updateRankingList(ranking)).toEqual(expectedAction)
  });
});