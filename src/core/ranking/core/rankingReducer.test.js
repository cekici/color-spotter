import * as ACTIONS from "../../../constants/actionTypes.js";
import RankingReducer from "./RankingReducer.js";

const initialState = {
  rankingList: [],
  isRankingEligible: null
};

describe("Ranking Reducer", () => {
  const reducer = new RankingReducer();

  it("should return the initial state", () => {
    expect(reducer(undefined, {}))
      .toEqual(initialState)
  })

  it("should handle START_GAME", () => {
    expect(reducer({
      ...initialState,
      isRankingEligible: true
    }, {
      type: ACTIONS.START_GAME
    })).toEqual(initialState)
  })

  it("should handle CHECK_RANKING_ELIGIBILITY", () => {
    expect(reducer({
      ...initialState
    }, {
      type: ACTIONS.CHECK_RANKING_ELIGIBILITY,
      payload: {
        score: 2
      }
    })).toEqual({
      ...initialState,
      isRankingEligible: true
    })

    const rankingList = [];

    for (let i = 0; i < 10; i++) {
      rankingList.push({score: 2});
    }

    expect(reducer({
      ...initialState,
      rankingList
    }, {
      type: ACTIONS.CHECK_RANKING_ELIGIBILITY,
      payload: {
        score: 1
      }
    })).toEqual({
      rankingList,
      isRankingEligible: false
    })
  })

  it("should handle UPDATE_RANKING_LIST", () => {
    const rankingList = [];

    for (let i = 0; i < 10; i++) {
      rankingList.push({score: 2});
    }

    expect(reducer({
      ...initialState,
      rankingList: [...rankingList]
    }, {
      type: ACTIONS.UPDATE_RANKING_LIST,
      payload: {
        ranking: {
          score: 1
        }
      }
    })).toEqual({
      isRankingEligible: null,
      rankingList: [...rankingList]
    })
  })
})