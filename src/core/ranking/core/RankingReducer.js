import * as ACTIONS from "../../../constants/actionTypes.js";
import {isEligableToEnterRankingList, updateRankingList} from "../../../utils/rankingUtils.js";

const defaultInitialState = {
  rankingList: [],
  isRankingEligible: null
};

function RankingReducer(initialState = defaultInitialState) {
  return function (state = initialState, action) {
    let newState;

    switch (action.type) {
      case ACTIONS.START_GAME:
        newState = {
          ...state,
          isRankingEligible: null
        };
        break;

      case ACTIONS.CHECK_RANKING_ELIGIBILITY:
        newState = {
          ...state,
          isRankingEligible: isEligableToEnterRankingList(state.rankingList, action.payload.score)
        };
        break;

      case ACTIONS.UPDATE_RANKING_LIST:
        newState = {
          isRankingEligible: null,
          rankingList: updateRankingList(state.rankingList, action.payload.ranking)
        };
        break;
      default:
        newState = state;
        break;
    }

    return newState;
  }
}

export default RankingReducer;
