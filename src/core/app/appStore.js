import {createStore, combineReducers} from "redux";

import {loadState, saveState} from "../../utils/persistanceUtils.js";
import GameReducer from "../game/core/GameReducer.js";
import RankingReducer from "../ranking/core/RankingReducer.js";

const persistedState = loadState();

const rootReducer = combineReducers({
  gameState: new GameReducer(),
  rankingState: new RankingReducer()
});

const store = createStore(
  rootReducer,
  persistedState
);

store.subscribe(() => {
  saveState(store.getState());
});

export default store;

