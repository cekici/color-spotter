const autoprefixer = require("autoprefixer");

module.exports = {
  plugins: function () {
    return [autoprefixer];
  }
};